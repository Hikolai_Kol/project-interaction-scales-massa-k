import socket
import sys
import binascii

PORT = 5001

HEADER = [0xf8, 0x55, 0xce]
LEN = (0x00, 0x01)

CMD_UDP_POLL = 0x00
CMD_TCP_GET_WEIGHT = 0xA0

IP = 'IP_ADDRESS_WEIGHING_TERMINAL'


def create_header(command: int):
    data = []
    data += HEADER
    data += LEN
    data.append(command)
    data += list(_crc16(data))
    return data

# Алгоритм расчета CRC (Версия алгоритма актуальна для версии Python 3)
def _crc16(data: bytearray, offset=0x0000, length=0x0001):
    if data is None or offset < 0 or offset > len(data) - 1 and offset+length > len(data):
        return 0
    crc = 0xFFFF
    for i in range(0, length):
        crc ^= data[offset + i] << 8
        for j in range(0, 8):
            if (crc & 0x8000) > 0:
                crc = (crc << 1) ^ 0x1021
            else:
                crc = crc << 1
    return (crc & 0xFFFF).to_bytes(2, sys.byteorder)


def find_weight():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.SOL_UDP) as suck:
        suck.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        suck.sendto(bytearray(create_header(CMD_UDP_POLL)), (IP, PORT))
        data, ip = suck.recvfrom(1024)
    return ip


def take_byte_info_weight():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0) as suck:
        suck.connect((IP, PORT))
        suck.send(bytearray(create_header(CMD_TCP_GET_WEIGHT)))
        data = suck.recv(1024)
    return binascii.hexlify(data, b'/')


def convert_weight_to_string(data):
    data[0], data[3] = data[3], data[0]
    data[1], data[2] = data[2], data[1]
    byte = str(''.join(data))
    n = int(byte, 16)
    if len(str(n)) > 3:
        n = str(n)[:-3] + '.' + str(n)[1:]
        return 'Вес товара: ' + n + ' кг.'
    elif len(str(n)) == 3:
        return 'Вес товара: ' + '0.' + str(n) + ' кг.'
    else:
        return 'Вес товара: ' + '0.' + str(n).rjust(3-len(str(n)), '0') + ' кг.'


def convert_devision_to_string(data):
    text = 'Цена деления: '
    if (data[0] == '01'):
        return text + '1 г'
    elif (data[0] == '00'):
        return text + '100 мг'
    elif (data[0] == '02'):
        return text + '10 г'
    elif (data[0] == '03'):
        return text + '100 г'
    elif (data[0] == '04'):
        return text + '1 кг'


def convert_stable_to_string(data):
    text = 'Признак стабилизации массы: '
    if (data[0] == '00'):
        return text + 'нестабилен'
    elif (data[0] == '01'):
        return text + 'стабилен'


if __name__ == '__main__':
    print("IP адрес кассы " + find_weight()[0])
    print("Порт кассы " + str(find_weight()[1]))
    byte_weight = take_byte_info_weight().decode().split('/')[6:][:4]
    byte_devision = take_byte_info_weight().decode().split('/')[10:][:1]
    byte_stable = take_byte_info_weight().decode().split('/')[11:][:1]
    print(convert_weight_to_string(byte_weight))
    print(convert_devision_to_string(byte_devision))
    print(convert_stable_to_string(byte_stable))
